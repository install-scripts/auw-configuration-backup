#!/bin/sh
# Script to backup the WAM database to the files on the Samba Share, there is another script that is called to
# copy the files to the external HDD
# Execute as wam user
cd /usr/wam3.0/rtdir/wam_p
. ~wam/.profile
echo " "
echo "#########################################################################"
echo " "
echo "Begin Backup of WAM `date`"
echo " "
mkdir /usr/wam3.0/backup/trx_files
mkdir /usr/wam3.0/backup/dsc
mkdir /usr/wam3.0/backup/form
rm -rf /usr/wam3.0/gui_samba/backup_files/trx_files
rm -rf /usr/wam3.0/gui_samba/backup_files/dsc
rm -rf /usr/wam3.0/gui_samba/backup_files/form
$idfl /cpy ORA:*.* TRX:
mv /usr/wam3.0/rtdir/wam_p/*.trx /usr/wam3.0/backup/trx_files/
cp -R /usr/wam3.0/online/dsc /usr/wam3.0/backup/
cp -R /usr/wam3.0/form /usr/wam3.0/backup/
cp -R /usr/wam3.0/backup/dsc /usr/wam3.0/gui_samba/backup_files
cp -R /usr/wam3.0/backup/form /usr/wam3.0/gui_samba/backup_files
cp -R /usr/wam3.0/backup/trx_files /usr/wam3.0/gui_samba/backup_files
rm -rf /usr/wam3.0/backup/trx_files
rm -rf /usr/wam3.0/backup/dsc
rm -rf /usr/wam3.0/backup/form
echo " "
echo "End Backup of WAM `date`"
echo " "
echo " ++++++++++++++++++++++++++++++++++++++++ "
echo " ++ running config_export +++++++++++++++ "
echo " ++++++++++++++++++++++++++++++++++++++++ "
/usr/wam3.0/bin/export_config_w7.sh
echo " ++ ALL DONE +++ "