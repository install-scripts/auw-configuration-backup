echo "starting export script"
cd /usr/wam3.0/rtdir/wam_p
echo "======================================"
echo " " 
echo " Starting with the settings inside the application " 
echo "test. rules and interface settings"
echo "======================================"
$idfl /cpy ORA:'AGROUP.*' TRX:
$idfl /cpy ORA:'AG.*' TRX:
$idfl /cpy ORA:'ANAEXSS.*' TRX:
$idfl /cpy ORA:'ANA.*' TRX:
$idfl /cpy ORA:'ARGROUP.*' TRX:
$idfl /cpy ORA:'ARIGHTS.*' TRX:
$idfl /cpy ORA:'ASE.*' TRX:
$idfl /cpy ORA:'BAT.*' TRX:
$idfl /cpy ORA:'CL.*' TRX:
$idfl /cpy ORA:'CLREP.*' TRX:
$idfl /cpy ORA:'CNT*.*' TRX:
$idfl /cpy ORA:'DISPATCH.*' TRX:
$idfl /cpy ORA:'ENVI*.*' TRX:
$idfl /cpy ORA:'ES.*' TRX:
$idfl /cpy ORA:'EVEN.*' TRX:
$idfl /cpy ORA:'EXSS1*.*' TRX:
$idfl /cpy ORA:'EXSS2*.*' TRX:
$idfl /cpy ORA:'EXSS3*.*' TRX:
$idfl /cpy ORA:'EXSS4*.*' TRX:
$idfl /cpy ORA:'EXSSDEF.*' TRX:
$idfl /cpy ORA:'EXSSRESM.*' TRX:
$idfl /cpy ORA:'EXSS.*' TRX:
$idfl /cpy ORA:'EXSS_VAL.*' TRX:
$idfl /cpy ORA:'GLEL.*' TRX:
$idfl /cpy ORA:'GLGROUP.*' TRX:
$idfl /cpy ORA:'GLL.*' TRX:
$idfl /cpy ORA:'GLOBREG.*' TRX:
$idfl /cpy ORA:'GST*.*' TRX:
$idfl /cpy ORA:'IM*.*' TRX:
$idfl /cpy ORA:'IPF*.*' TRX:
$idfl /cpy ORA:'MICCNT.*' TRX:
$idfl /cpy ORA:'METHOD.*' TRX:
$idfl /cpy ORA:'METHODWP.*' TRX:
$idfl /cpy ORA:'MOLIS*.*' TRX:
$idfl /cpy ORA:'MOL_*.*' TRX:
$idfl /cpy ORA:'MOLPROC.*' TRX:
$idfl /cpy ORA:'MOLQ*.*' TRX:
$idfl /cpy ORA:'MOLXPAR.*' TRX:
$idfl /cpy ORA:'MPL00.*' TRX:
$idfl /cpy ORA:'MPLELEX.*' TRX:
$idfl /cpy ORA:'MPLEL.*' TRX:
$idfl /cpy ORA:'MPLELUSR.*' TRX:
$idfl /cpy ORA:'MPLGROUP.*' TRX:
$idfl /cpy ORA:'MPLGRENT.*' TRX:
$idfl /cpy ORA:'MSITE.*' TRX:
$idfl /cpy ORA:'MTRACE.*' TRX:
$idfl /cpy ORA:'MSGREP.*' TRX:
$idfl /cpy ORA:'MWS*.*' TRX:
$idfl /cpy ORA:'OPALDEF.*' TRX:
$idfl /cpy ORA:'PARDEF.*' TRX:
$idfl /cpy ORA:'PRATT.*' TRX:
$idfl /cpy ORA:'PRODUCT.*' TRX:
$idfl /cpy ORA:'PRT*.*' TRX:
$idfl /cpy ORA:'REF*.*' TRX:
$idfl /cpy ORA:'REGRP.*' TRX:
$idfl /cpy ORA:'RM*.*' TRX:
$idfl /cpy ORA:'RPT*.*' TRX:
$idfl /cpy ORA:'ROUTE.*' TRX:
$idfl /cpy ORA:'RULES_ELEMENT.*' TRX:
$idfl /cpy ORA:'RULES.*' TRX:
$idfl /cpy ORA:'RULES_TST_RES.*' TRX:
$idfl /cpy ORA:'SESS.*' TRX:
$idfl /cpy ORA:'USER.*' TRX:
$idfl /cpy ORA:'USR.*' TRX:
$idfl /cpy ORA:'VAL.*' TRX:
$idfl /cpy ORA:'VERSION.*' TRX:
$idfl /cpy ORA:'WL*.*' TRX:
$idfl /cpy ORA:'WORK*.*' TRX:
$idfl /cpy ORA:'WP*.*' TRX:
echo "================================================"
echo "== All exported                               =="
echo "================================================"
echo "==  Compressing now                           =="
echo "================================================"
tar -czvf all_config_trx.tgz *.trx

echo "================================================"
echo "==  saving interface DSC files                =="
echo "================================================"

cd /usr/wam3.0/online/dsc

rm all_dsc_file.tgz
tar -czvf all_dsc_file.tgz *.dsc
mv all_dsc_file.tgz /usr/wam3.0/gui_samba

cd /usr/wam3.0/rtdir/wam_p


echo "================================================"
echo "==  now saving other various usefull files    =="
echo "================================================"

rm *.trx

mkdir save_other_files
cd save_other_files
cp /usr/wam3.0/usys8401/license.dat .
cp /usr/wam3.0/rtdir/common/*.asn .
cp /usr/wam3.0/rtdir/wam_p/local*.asn .
cd ..
rm all_other_files.tgz
tar -czvf all_other_files.tgz save_other_files/*.*

echo "================================================"
echo "=== moving files to gui_samba                ==="
echo "================================================"

mv all_*.tgz /usr/wam3.0/gui_samba

echo "================================================"
echo "===  the backupfiles are now available in    ==="
echo "===  /usr/wam3.0/gui_samba                   ==="
echo "===  all_config_trx.tgz, all_dsc_file.tgz and all_other_files.tgz    ==="
echo "===  the backupfiles are now available in    ==="
echo "===  the backupfiles are now available in    ==="
echo "================================================"