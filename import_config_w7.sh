#  Script to restore the customer configuration of the 
#	UFWAM
#  it reads the data from the gui_samba location 
cd /usr/wam3.0/rtdir/wam_p  
. ~wam/.profile

echo "stopping all "
molis_admin stopall  &>/dev/null
echo "----------------------------------------------------------------------"
echo "Stop all done"
echo "----------------------------------------------------------------------"

echo " " 
echo "==================================================="
echo " Starting with the Settings inside the application " 
echo " test, rules and interface settings"
echo "==================================================="

cd /usr/wam3.0/rtdir/wam_p   &>/dev/null

cp /usr/wam3.0/gui_samba/all*tgz .  &>/dev/null

echo "================================"
echo "== Question about IPU version =="
echo "================================"
echo -n "Are you updating the IPU version from <= v16 to >=v16 (y/n)?"
old_stty_cfg=$(stty -g)
stty raw -echo
ipuAnswer=$( while ! head -c 1 | grep -i '[ny]' ;do true ;done )
stty $old_stty_cfg

rm -f /tmp/DbRules_clean.sql
echo "truncate table RULES;" >> /tmp/DbRules_clean.sql
echo "truncate table RULES_ELEMENT;" >> /tmp/DbRules_clean.sql
echo "truncate table RULES_TST_RES;" >> /tmp/DbRules_clean.sql
echo "commit;" >> /tmp/DbRules_clean.sql
echo "exit;" >> /tmp/DbRules_clean.sql
sqlplus wam/abc123 @/tmp/DbRules_clean.sql



rm *.trx
echo "===================================================="
echo " now restoring settings "
echo "===================================================="

tar -xzvf /usr/wam3.0/gui_samba/all_config_trx.tgz  &>/dev/null
$idfl /cpy TRX:'*.trx' ORA: 

if echo "$ipuAnswer" | grep -iq "^y" ;then
	echo " ++++++++++++++++++++++++++++++++++++++++++++++++"
	echo " ++ now importing IPUversion specific code     ++"
	echo " ++++++++++++++++++++++++++++++++++++++++++++++++"
	$idfl /cpy TRX:"/usr/wam3.0/bin/*.trx" ORA:
fi

echo "===================================================="
echo " now restoring dsc files "
echo "===================================================="

echo "now restoring all the interface dsc files"
rm *.dsc  &>/dev/null
tar -xzvf /usr/wam3.0/gui_samba/all_dsc_file.tgz   &>/dev/null
cp *.dsc ../../online/dsc  &>/dev/null

echo "==================================================="
echo " now restoring various other usefull files, license file and asn files mainly"
rm save_other_files/*.*  &>/dev/null
rmdir save_other_files  &>/dev/null
tar -xzvf /usr/wam3.0/gui_samba/all_other_files.tgz 

cd save_other_files

cp license.dat /usr/wam3.0/usys8401
mv local*.asn /usr/wam3.0/rtdir/wam_p  &>/dev/null
cp *.asn /usr/wam3.0/rtdir/common  &>/dev/null
cd ..
echo "==================================================="
echo " Done with restore"

echo "starting all Linux SHM and interfaces "
molis_admin startall  &>/dev/null
echo "----------------------------------------------------------------------"
echo "Startall done"
echo "----------------------------------------------------------------------"




